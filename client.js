const net = require("net");
const prompt = require("prompt-sync")();
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let choosePort = prompt("Choose wich port you want to connect to: ");
let username = prompt("Choose username: ");

const client = net.createConnection({
  port: choosePort,
});

client.on("connect", () => {
  client.write(`${username} connected to the server!`);
});

rl.on("line", (data) => {
  if (data === "quit") {
    client.write(`${username}: left the chat!`);
    client.end();
  } else client.write(`${username}: ${data}`);
});

client.on("data", (data) => {
  console.log(`${data}`);
});

client.on("end", () => {
  console.log(`${username}: left the chat!`);
});
