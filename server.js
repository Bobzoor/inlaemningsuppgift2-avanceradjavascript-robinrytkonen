const net = require("net");

let clients = [];

const server = net.createServer((client) => {
  clients.push(client);
  console.log("client connected");

  client.on("data", (data) => {
    broadcast(data, client);
  });

  client.on("close", () => {
    console.log("A client has left the chat.");
  });
});

server.listen(8080, "localhost", () => {
  console.log("Opened server on port:", server.address().port);
});

function broadcast(message, clientSent) {
  if (message === "quit") {
    const index = clients.indexOf(clientSent);
    clients.splice(index, 1);
  } else {
    clients.forEach((client) => {
      if (client !== clientSent) client.write(message);
    });
  }
}

// If another server is already listening on the given port.
server.on("error", (e) => {
  if (e.code === "EADDRINUSE") {
    console.log("Address in use, retrying...");
    setTimeout(() => {
      server.close();
      server.listen(8080, "localhost");
    }, 5000);
  }
});
